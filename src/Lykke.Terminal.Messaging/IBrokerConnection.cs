﻿using System;

namespace Lykke.Terminal.Messaging
{
    public interface IBrokerConnection : IDisposable
    {
        void Start();
        void Disconnect();
    }
}