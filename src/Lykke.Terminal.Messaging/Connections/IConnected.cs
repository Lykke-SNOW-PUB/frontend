﻿namespace Lykke.Terminal.Messaging.Connections
{
    public interface IConnected<out T>
    {
        T Value { get; }
        bool IsConnected { get; }
    }
}