﻿using System;

namespace Lykke.Terminal.Messaging
{
    public interface IBroker
    {
        IObservable<T> SubscribeToTopic<T>(string topic);
        void PublishToTopic<T>(string topic, T payload);
    }
}