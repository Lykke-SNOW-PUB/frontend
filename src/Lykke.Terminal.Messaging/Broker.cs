﻿using System;
using System.Threading.Tasks;
using System.Xml;
using SystemEx;
using WampSharp.V2;
using WampSharp.V2.MetaApi;

namespace Lykke.Terminal.Messaging
{
    public class Broker : IBroker, IDisposable
    {
        private readonly IWampChannel _channel;
        private readonly WampMetaApiServiceProxy _meta;

        public Broker(IWampChannel channel)
        {
            _channel = channel;
            _meta = _channel.RealmProxy.GetMetaApiServiceProxy();
        }

        public IObservable<T> SubscribeToTopic<T>(string topic)
        {
            return
                _channel.RealmProxy.Services.GetSubject<T>(topic);
        }

        public void PublishToTopic<T>(string topic, T value)
        {
            var subject = _channel.RealmProxy.Services.GetSubject<T>(topic);
            subject.OnNext(value);
        }

        public Task<IAsyncDisposable> RegisterCallee(object calleeInstance)
        {
            return
                _channel.RealmProxy.Services.RegisterCallee(calleeInstance);
        }

        public void Dispose()
        {
        }
    }
}