﻿using System;
using System.Reactive.Disposables;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using Lykke.Terminal.Messaging.Connections;
using WampSharp.V2;
using WampSharp.V2.Client;
using WampSharp.V2.Fluent;

namespace Lykke.Terminal.Messaging
{
    public class BrokerConnection : IBrokerConnection
    {
        private readonly WampChannelReconnector _reconnector;
        private readonly SerialDisposable _sessionDispose = new SerialDisposable();

        private readonly BehaviorSubject<IConnected<IBroker>> _connectedSubject =
            new BehaviorSubject<IConnected<IBroker>>(Connected.No<IBroker>());

        public IWampChannel Channel { get; }

        public BrokerConnection(string uri, string realm)
        {
            Channel = new WampChannelFactory()
                .ConnectToRealm(realm)
                .WebSocketTransport(uri)
                .JsonSerialization()
                .Build();

            Func<Task> connect = async () =>
            {
                try
                {
                    await Channel.Open();
                    _connectedSubject.OnNext(Connected.Yes(new Broker(Channel)));
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                    _connectedSubject.OnNext(Connected.No<IBroker>());
                    throw;
                }
            };

            _reconnector = new WampChannelReconnector(Channel, connect);
        }

        public void Start()
        {
            _reconnector.Start();
        }

        public void Disconnect()
        {
            _sessionDispose.Dispose();
            _reconnector.Dispose();
        }

        public void Dispose()
        {
            _reconnector.Dispose();
        }
    }
}