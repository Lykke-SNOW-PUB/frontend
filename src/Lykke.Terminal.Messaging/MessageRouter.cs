﻿using System;
using System.Threading.Tasks;

namespace Lykke.Terminal.Messaging
{
    public class MessageRouter : IMessageRouter
    {
        private IBroker _broker;
        private IServiceBusListener _serviceBusListener;

        public MessageRouter()
        {

        }

        public MessageRouter(IBroker broker, IServiceBusListener serviceBusListener)
        {
            _broker = broker;
            _serviceBusListener = serviceBusListener;
        }

        public MessageRouter RegisterReceiver(IServiceBusListener serviceBusListener)
        {
            _serviceBusListener = serviceBusListener;
            return this;
        }

        public MessageRouter RegisterPusher(IBroker broker)
        {
            _broker = broker;
            return this;
        }

        public async Task Forward<T>(string pullTopic, string pushTopic)
        {
            await _serviceBusListener.SubscribeAsync<T>(pullTopic,
                async (message, context) =>
                {
                    await Task.Run(
                        () =>
                        {
                            Console.WriteLine("Recieved object {0}", message);
                            _broker.PublishToTopic<T>(pushTopic, message);
                        });
                });
        }

        public void PullAndPush<TInput, TOutput>(string pullTopic, string pushTopic)
        {
            _serviceBusListener.SubscribeTo<TInput>(pullTopic,
                (message) =>
                {
                    _broker.PublishToTopic<TInput>(pushTopic, message);
                });
        }
    }
}