﻿using System;
using System.Threading.Tasks;
using RawRabbit;
using RawRabbit.Configuration;
using RawRabbit.Configuration.Exchange;
using RawRabbit.Context;
using RawRabbit.vNext;

namespace Lykke.Terminal.Messaging
{
    public class RabbitMqListener : IServiceBusListener
    {
        private readonly IBusClient _busClient;

        public RabbitMqListener(RawRabbitConfiguration config)
        {
            _busClient = BusClientFactory.CreateDefault(config);
        }

        public async Task SubscribeAsync<T>(string topic,
            Func<T, MessageContext, Task> messageCallbackAsyncFunc)
        {
            try
            {
                await Task.Run(() =>
                        _busClient.SubscribeAsync<T>(
                            async (message, context) => await messageCallbackAsyncFunc(message, context),
                            cfg => cfg
                                .WithExchange(e =>
                                        e.WithName(topic).WithType(ExchangeType.Topic))
                                .WithRoutingKey("Terminal"))
                );
            }
            catch (Exception exception)
            {
                Console.WriteLine("[TerminalApplication] [Router] [Unable to create subscription] [Exception: {0}",
                    exception);
            }
        }

        public void SubscribeTo<T>(string topic, Action<T> messageHandler)
        {
            throw new NotImplementedException();
        }
    }
}