﻿using System;

namespace Lykke.Terminal.Messaging
{
    public class BrokerUri
    {
        public static string FromConfig(IBrokerConfiguration config)
        {
            return new UriBuilder
            {
                Scheme = "ws",
                Path = "ws",
                Port = config.Port,
                Host = config.Host
            }.ToString();
        }
    }
}