﻿namespace Lykke.Terminal.Messaging
{
    public interface IMessageRouter
    {
        void PullAndPush<TInput, TOutput>(string pullTopic, string pushTopic);
    }
}