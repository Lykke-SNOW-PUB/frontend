﻿using RawRabbit.Configuration;

namespace Lykke.Terminal.Messaging
{
    public interface IServiceBusConfiguration
    {
        
    }

    public class RabbitMqConfiguration : RawRabbitConfiguration, IServiceBusConfiguration
    {
    }
}