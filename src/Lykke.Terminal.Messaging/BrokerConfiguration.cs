﻿using Microsoft.Extensions.Configuration;

namespace Lykke.Terminal.Messaging
{
    public class BrokerConfiguration : IBrokerConfiguration
    {
        public BrokerConfiguration(IConfiguration config)
        {
            Port = int.Parse(config["port"]);
            Host = config["host"];
            Realm = config["realm"];
        }

        public int Port { get; set; }
        public string Host { get; set; }
        public string Realm { get; set; }
    }
}