﻿namespace Lykke.Terminal.Messaging
{
    public interface IBrokerConfiguration
    {
        int Port { get; set; }
        string Host { get; set; }
        string Realm { get; set; }
    }
}