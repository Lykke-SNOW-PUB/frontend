﻿using System;
using System.Threading.Tasks;
using RawRabbit.Context;

namespace Lykke.Terminal.Messaging
{
    public interface IServiceBusListener
    {
        void SubscribeTo<T>(string topic, Action<T> messageHandler);

        Task SubscribeAsync<T>(string topic,
            Func<T, MessageContext, Task> messageCallbackAsyncFunc);
    }
}