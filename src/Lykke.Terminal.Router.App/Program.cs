﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Lykke.Terminal.Common.Extenstions;
using Lykke.Terminal.Domain.Accounts;
using Lykke.Terminal.Domain.Exchange;
using Lykke.Terminal.Domain.Settings;
using Lykke.Terminal.Infrastructure.Concerns.RabbitMq;
using Lykke.Terminal.Messaging;
using Lykke.Terminal.BusinessService.Proxy.MatchingEngine;
using Lykke.Terminal.BusinessService.Proxy;
using Lykke.Terminal.BusinessService.Proxy.BlotterService;
using Lykke.Terminal.BusinessService.Proxy.TradeExecution;
using Lykke.Terminal.Common.Log;
using Microsoft.Extensions.Configuration;

namespace Lykke.Terminal.Router.App
{
    public class Program
    {
        private static IHttpServiceProxy _webServicesProxy;
        private static IHttpServiceProxy _matchingEngineProxy;

        public static void Main(string[] args)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddEnvironmentVariables()
                .AddJsonFile("appsettings.json", true)
                .Build();

            var serviceBusConfig = config.GetSection("RawRabbit");
            var rawRabbitConfig = serviceBusConfig.Bind<RabbitMqConfiguration>();

            var webServicesEndpoint = config.GetAsString("WebServicesEndpoint");
            var matchingEngineEndpoint = config.GetAsString("MatchingEngineEndpoint");

            _webServicesProxy = new HttpServiceProxy(webServicesEndpoint);
            _matchingEngineProxy = new HttpServiceProxy(matchingEngineEndpoint);

            using (
                var brokerConnection =
                    BrokerConnectionFactory.Create(new BrokerConfiguration(config.GetSection("broker"))))
            {
                brokerConnection.Start();

                Task.Run(async () =>
                {
                    await new MessageRouter()
                        .RegisterReceiver(new RabbitMqListener(rawRabbitConfig))
                        .RegisterPusher(new Broker(brokerConnection.Channel))
                        .Forward<AssetPairQuote>(KnownTopics.AssetPairPriceUpdated, KnownTopics.AssetPairPriceUpdated);

                    await new MessageRouter()
                        .RegisterReceiver(new RabbitMqListener(rawRabbitConfig))
                        .RegisterPusher(new Broker(brokerConnection.Channel))
                        .Forward<AccountInfo>(KnownTopics.AccountUpdated, KnownTopics.AccountUpdated);

                    await new Broker(brokerConnection.Channel)
                        .RegisterCallee(new MatchingEngineProxy(_webServicesProxy))
                        .ConfigureAwait(false);

                    await new Broker(brokerConnection.Channel)
                        .RegisterCallee(new BlotterService(_webServicesProxy))
                        .ConfigureAwait(false);

                    await new Broker(brokerConnection.Channel)
                        .RegisterCallee(
                            new TradeExecutionServiceProxy(
                                new HttpServiceProxy(matchingEngineEndpoint), new LogToConsole(), _webServicesProxy, new Broker(brokerConnection.Channel)))
                        .ConfigureAwait(false);
                });
            }

            Console.ReadLine();
        }
    }
}