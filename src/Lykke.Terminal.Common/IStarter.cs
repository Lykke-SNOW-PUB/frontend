﻿namespace Lykke.Terminal.Common
{
    public interface IStarter
    {
        void Start();
    }
}