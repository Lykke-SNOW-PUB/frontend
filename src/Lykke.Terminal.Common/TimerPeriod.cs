﻿using System;
using System.Threading.Tasks;
using Lykke.Terminal.Common.Log;

namespace Lykke.Terminal.Common
{
    // Таймер, который исполняет метод Execute через определенный интервал после окончания исполнения метода Execute
    public abstract class TimerPeriodBase : IStarter
    {
        private readonly string _componentName;
        private readonly int _periodMs;
        private readonly ILog _log;

        protected TimerPeriodBase(string componentName, int periodMs, ILog log)
        {
            _componentName = componentName;

            _periodMs = periodMs;
            _log = log;
        }

        public bool Working { get; private set; }

        private void LogFatalError(Exception exception)
        {
           _log?.WriteFatalError(_componentName, "Loop", "", exception).Wait();
        }


        private async void ThreadMethod()
        {
            while (Working)
            {
                try
                {
                    await Execute();
                }
                catch (Exception exception)
                {
                    LogFatalError(exception);
                }
                await Task.Delay(_periodMs);
            }
        }

        protected abstract Task Execute();


        private readonly object _startLockObject = new object();
        public virtual void Start()
        {

            lock(_startLockObject)
            {
                if (Working)
                    return;

                Working = true;
            }

            ThreadMethod();
        }

        public void Stop()
        {
            Working = false;
        }

    }

    public class TimerPeriod: TimerPeriodBase
    {
        private readonly Func<Task> _callback;

        public TimerPeriod(string componentName, int periodMs, ILog log, Func<Task> callback) : base(componentName, periodMs, log)
        {
            _callback = callback;
        }

        protected sealed override Task Execute()
        {
            return _callback();
        }
    }

}
