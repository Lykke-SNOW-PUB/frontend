﻿using Microsoft.Extensions.Configuration;

namespace Lykke.Terminal.Common.Extenstions
{
    public static class ConfigurationExtensions
    {
        public static string GetAsString(this IConfiguration config, string key)
        {
            return config[key];
        }

        public static int GetAsInt(this IConfiguration config, string key)
        {
            return int.Parse(config[key]);
        }
    }
}