﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Lykke.Terminal.Common.Extenstions
{
    public static class HttpClientExtensions
    {
        public static async Task<T> GetAsObjectAsync<T>(this HttpClient httpClient, string resourcePath)
        {
            var response = await httpClient.GetStringAsync(resourcePath);
            return
                await Task.Factory.StartNew<T>(() =>
                        JsonConvert.DeserializeObject<T>(response));
        }

        public static async Task<HttpResponseMessage> PostAsJsonAsync(this HttpClient httpClient, string resourcePath,
            object payload)
        {
            return
                await httpClient.PostAsync(resourcePath,
                    new StringContent(
                        JsonConvert.SerializeObject(payload),
                        Encoding.UTF8,
                        "application/json"
                    )
                );
        }

        public static async Task<HttpResponseMessage> PutAsJsonAsync(this HttpClient httpClient, string resourcePath,
            object payload)
        {
            return
                await httpClient.PutAsync(resourcePath,
                    new StringContent(
                        JsonConvert.SerializeObject(payload),
                        Encoding.UTF8,
                        "application/json"
                    )
                );
        }
    }
}
