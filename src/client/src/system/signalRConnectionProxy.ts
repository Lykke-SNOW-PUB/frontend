import { IConnection, ISessionProxy, SignalRSessionProxy } from './index';
import 'jquery';
import 'signalr';

/**
 * Backend connection implemented with SignalR
 */
export default class SignalRConnectionProxy implements IConnection {
  private hubName: string = 'tradingHub';
  private hubConnection: SignalR.Hub.Connection;
  private hubProxy: SignalR.Hub.Proxy;

  session: ISessionProxy;

  constructor(private url: string) {
  }

  open() {
    this.hubConnection = $.hubConnection(this.url);
    this.hubProxy = this.hubConnection.createHubProxy(this.hubName);

    this.session = new SignalRSessionProxy(this.hubProxy);

    this.hubConnection.start()
      .done(() =>
        console.log(`[BNP] Connected to hub: ${this.hubName}`))
      .fail(() =>
        console.error(`[BNP] Connection to hub failed: ${this.hubName}`));
  }

  close() {
    this.hubConnection.stop();
  }

  public call(event: string, data: any) {
    return this.hubProxy.invoke(event, data);
  }
}
