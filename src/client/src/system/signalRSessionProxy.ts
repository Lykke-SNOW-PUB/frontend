import { ISessionProxy } from './index';
import 'signalr';

/**
 * SignalRSessionProxy implements ISubscriptionProxy
 */
class SignalRSessionProxy implements ISessionProxy {
  constructor(private session: SignalR.Hub.Proxy) {
  }

  subscribe(event, eventHandler): void {
    this.session.on(event, eventHandler)
  }

  unsubscribe(event) {
    this.session.off(event, () => { });
  }

  publish(topic: string, data: any[]) {
    throw "Not implemented";
  }

  call(name, data) {
    return When.promise((resolve, reject) =>
      this.session.invoke(name, data)
        .then(resolve, reject));
  }
}

export default SignalRSessionProxy;
