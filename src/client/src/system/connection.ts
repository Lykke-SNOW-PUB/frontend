import { NetworkTransport, ConnectionProvider } from './index';

export interface IConnection {
  open(): void;
  close(): void;
}

export interface ISessionProxy {
  subscribe: (topic: string, cb: (args: any[], kwargs: any) => void) => void;
  unsubscribe: (topic: string) => void;
  publish: (topic: string, data: any[]) => void;
  call: (topic: string, payload?: any[], method?: string) => When.Promise<any>;
}

export interface IConnectionProxy extends IConnection {
  session: ISessionProxy;
  getTransportInfo(): NetworkTransport;
  getName(): any;
  onOpen(callback: (session: autobahn.Session, details) => void): void
}

export interface IObservableConnection extends IConnection {
  subscribeTo(event: string, eventHandler: (dto) => void);
  unsubscribeFrom(event: string)
}

/**
 * Connection to the backend systems abstraction
 */
export default class Connection implements IObservableConnection {
  private _isConnected: boolean;

  constructor(private connectionProxy: IConnectionProxy, private reconnectionPolicy?, private userName?: string) {
    this.userName = "john_doe";
    this._isConnected = false;
  }

  public get isConnected(): boolean {
    return this._isConnected;
  }

  public getTransportInfo(): NetworkTransport {
    return this.connectionProxy.getTransportInfo();
  }

  public getConnectionProvider(): any {
    return this.connectionProxy.getName();
  }

  public open(): Connection {
    if (!this._isConnected) {
      this.connectionProxy.open();
      this._isConnected = true;
      return this;
    }
  }

  public close(): void {
    if (this._isConnected) {
      this.connectionProxy.close();
      this._isConnected = false;
    }
  }

  public subscribeTo(topic: string, eventHandler: (dto) => void): void {
    this.connectionProxy.session.subscribe(topic, eventHandler);
  }

  public unsubscribeFrom(topic: string): void {
    this.connectionProxy.session.unsubscribe(topic);
  }
}
