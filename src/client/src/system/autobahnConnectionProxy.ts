import { IConnectionProxy, ISessionProxy, NetworkTransport, ConnectionProvider, AutobahnSessionProxy } from './index';
import * as autobahn from 'autobahn';
import { logger } from './index';

/**
 * AutobahnConnectionProxy
 */
class AutobahnConnectionProxy implements IConnectionProxy {
  private log;
  private connection: autobahn.Connection;
  public session: ISessionProxy;

  private _onOpen: (session, details) => void;
  private _onClose: (reason, details) => void;

  constructor(private url: string, private realm: string) {
    this.connection = new autobahn.Connection({
      url: this.url,
      realm: this.realm
    });
    this.log = logger.create('AutobahnConnectionProxy');
  }

  open() {
    this.connection.onopen = (session: autobahn.Session, details) => {
      this.session = new AutobahnSessionProxy(session);
      this.log.debug('Connected to Crossbar', details);
      if (this._onOpen != undefined) {
        this._onOpen(session, details);
      }
    };

    this.connection.onclose = (reason, details) => {
      this.log.debug('Crossbar connection closed', reason, details);
      if (this._onClose !== undefined) {
        this._onClose(reason, details);
      }

      return true;
    }

    this.connection.open();
  }

  close() {
    this.connection.close();
  }

  onOpen(callback: (session: autobahn.Session, details) => void): void {
    this._onOpen = callback;
  };

  onClose(callback: (session: autobahn.Session, details) => void): void {
    this._onClose = callback;
  }

  getTransportInfo() {
    return NetworkTransport.WebSockets;
  }

  getName() {
    return ConnectionProvider.Autobahn;
  }
}

export default AutobahnConnectionProxy;
