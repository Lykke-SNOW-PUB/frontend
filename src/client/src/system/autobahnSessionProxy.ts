import * as autobahn from 'autobahn';
import * as when from 'when';

/**
 * AutobahnSessionProxy: makes the autobahn session api more explicit, aids testing
 */
class AutobahnSessionProxy {
  private session: autobahn.Session;
  private subscriptions: autobahn.ISubscription[];

  constructor(session: autobahn.Session) {
    this.session = session;
    this.subscriptions = [];
  }

  subscribe(topic: string, onResults: (args: any[], kwargs?: any) => void): when.Promise<any> {
    return this.session
      .subscribe(topic, onResults)
      .then(subscription => {
        this.subscriptions.push(subscription);
      });
  }

  unsubscribe(subscription: string): when.Promise<any> {
    return this.session.unsubscribe(
      this.subscriptions.find(s => s.topic === subscription)
    );
  }

  publish(topic: string, data: any[]) {
    return this.session.publish(topic, data, {}, {
      eligible: [this.session.id]
    })
  }

  call(procedure: string, params: any[]): when.Promise<any> {
    return this.session.call(procedure, params);
  }
}

export default AutobahnSessionProxy;
