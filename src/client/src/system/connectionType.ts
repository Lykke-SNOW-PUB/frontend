enum NetworkTransport {
  WebSockets,
  ServerSentEvents,
  LongPolling
}

export default NetworkTransport;
