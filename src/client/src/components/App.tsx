import * as React from 'react';
import * as esp from 'esp-js';
import { RouterProvider, SmartComponent } from 'esp-js-react';
import router from '../system/router';
import { Header } from './shared/header/index';
import { Shell } from './shared/shell/index';
import { CreateOrderModalView } from './orders/views/index';

export default () => (
  <RouterProvider router={router}>
    <div>
      <Header />
      <SmartComponent modelId='shellViewModelId' view={Shell} />
      <SmartComponent modelId='createOrderViewModelId' view={CreateOrderModalView} />
    </div>
  </RouterProvider>
);
