import * as React from 'react';
import { SmartComponent } from 'esp-js-react';
import { IObservableViewProps } from '../../base/views/index';
import { InstrumentsViewModel } from '../model/index';
import { EventTypes } from '../../../services/events/index';

import './instruments.css';

class InstrumentsView extends React.Component<IObservableViewProps<InstrumentsViewModel>, { showCreateOrder: boolean }> {
  constructor() {
    super();
    this.state = {
      showCreateOrder: false
    };
    this.tradeClicked = this.tradeClicked.bind(this);
    this.createOrderDismissed = this.createOrderDismissed.bind(this);
  }

  tradeClicked(quote) {
    this.props.router.publishEvent('createOrderViewModelId', EventTypes.OPEN_CREATE_ORDER_MODAL, { quote: quote });
  }

  createOrderDismissed(evt) {
    this.setState({
      showCreateOrder: false
    });
  }

  render() {
    let instruments = this.props.model;
    let router = this.props.router;

    return (
      <div>
        <table className="table instruments">
          <thead>
            <tr>
              <th className="instrument__heading instrument__cell_left-aligned">+/-</th>
              <th className="instrument__heading instrument__cell_left-aligned">Name</th>
              <th className="instrument__heading">Sell</th>
              <th className="instrument__heading">Buy</th>
              <th className="instrument__heading">Change</th>
              <th className="instrument__heading">in %</th>
              <th className="instrument__heading">Trade</th>
              <th className="instrument__heading">Del</th>
            </tr>
          </thead>
          <tbody>
            {instruments.quotes.map(quote => {
              let upOrDown = true;
              let cssClass = upOrDown ? "instrument_up" : "instrument_down";
              return (
                <tr key={quote.symbol}>
                  <td className="instrument__data">&nbsp;</td>
                  <td className={"instrument__data instrument__cell_left-aligned " + cssClass}>{quote.symbol}&nbsp;<span className="caret"></span></td>
                  <td className="instrument__data">{quote.sell}</td>
                  <td className="instrument__data">{quote.buy}</td>
                  <td className="instrument__data">{quote.change}</td>
                  <td className="instrument__data">{quote.changeRate}</td>
                  <td className="instrument__data">
                    <button type="button" className="btn btn-trade"
                      onClick={() => this.tradeClicked(quote)}
                      data-asset="{AssetPairId}" data-ask="{Ask}" data-bid="{Bid}">Trade</button></td>
                  <td className="instrument__data">
                    <span className="glyphicon glyphicon-trash" aria-hidden="true"></span></td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default InstrumentsView;
