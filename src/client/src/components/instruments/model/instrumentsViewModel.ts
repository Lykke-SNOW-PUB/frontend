import * as esp from 'esp-js';
import { ViewModelBase } from '../../base/model/index';
import { EventTypes } from '../../../services/events/index';
import { QuoteViewModel } from '../model/index';

class InstrumentsViewModel extends ViewModelBase {
  quotes: QuoteViewModel[];

  constructor(router: esp.Router) {
    super("instrumentsViewModelId", router);
    this.quotes = [];
  }

  @esp.observeEvent(EventTypes.INIT_QUOTES)
  onInitQuotes(event) {
    this.quotes = [];
    for (let quoteDto of event.quotes) {
      this.quotes.push(
        QuoteViewModel.fromDto(quoteDto)
      );
    }
  }

  @esp.observeEvent(EventTypes.UPDATE_QUOTE)
  onUpdateQuote(event) {
    for (let quoteDto of event.quote) {
      let idx = this.quotes.findIndex(q => q.symbol == quoteDto.AssetPairId);
      if (idx > -1) {
        this.quotes[idx] = QuoteViewModel.fromDto(quoteDto);
      }
    }
  }
}

export default InstrumentsViewModel;
