class QuoteViewModel {
  symbol: string;
  buy: string;
  sell: string;
  change: string;
  changeRate: string;

  static fromDto(quoteDto: any): QuoteViewModel {
    let quote = new QuoteViewModel();

    quote.symbol = quoteDto.AssetPairId;
    quote.buy = quoteDto.Bid.toFixed(3);
    quote.sell = quoteDto.Ask.toFixed(3);
    quote.change = Math.random().toFixed(2);
    quote.changeRate = Math.random().toFixed(3);

    return quote;
  }
}

export default QuoteViewModel;
