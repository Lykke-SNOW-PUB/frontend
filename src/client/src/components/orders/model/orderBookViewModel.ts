import * as esp from 'esp-js';
import { ViewModelBase } from '../../base/model/index';
import { EventTypes } from '../../../services/events/index';

class OrderBookItemViewModel {
  Symbol: string;
  BuyOrders: OrderBookItemPositionViewModel[];
  SellOrders: OrderBookItemPositionViewModel[];
}

class OrderBookItemPositionViewModel {
  Volume: number;
  Price: number;
}

/**
 * ViewModel for order book
 */
class OrderBookViewModel extends ViewModelBase {
  items: OrderBookItemViewModel[];

  constructor(router: esp.Router) {
    super('orderBookViewModelId', router);
    this.items = [];
  }

  @esp.observeEvent(EventTypes.ORDER_BOOK_UPDATED)
  private onOrderBookUpdated(event) {
    this.items = event.items;
  }
}

export default OrderBookViewModel;
