class OrderViewModel {
  id: string;
  symbol: string;
  direction: string;
  quantity: number;
  orderType: string;
  definedPrice: number;
  createdAt: Date;

  static fromDto(orderDto: any): OrderViewModel {
    let orderViewModel = new OrderViewModel();

    orderViewModel.id = orderDto.Uid;
    orderViewModel.symbol = orderDto.AssetPairId;
    // orderViewModel.direction = orderDto.volume < 0 ? "Sell" : "Buy";
    orderViewModel.quantity = orderDto.Volume;
    orderViewModel.orderType = 42 > 0 ? "Limit" : "Market";
    orderViewModel.definedPrice = orderDto.definedPrice || 0;
    orderViewModel.createdAt = orderDto.CreatedAt;

    return orderViewModel;
  }
}

export default OrderViewModel;
