import * as esp from 'esp-js';
import { ViewModelBase } from '../../base/model/index';
import { EventTypes } from '../../../services/events/index';
import { IObservableConnection, IConnectionProxy, AutobahnConnectionProxy, logger } from '../../../system/index';

import { mockUser } from '../../../api/index';

const user = mockUser();
const _logger = logger.create('AppBootstrapper');

class CreateOrderViewModel extends ViewModelBase {
  showModal: boolean;

  symbol: string;
  buy: number;
  sell: number;

  constructor(router: esp.Router, private connectionProxy?: IConnectionProxy) {
    super("createOrderViewModelId", router);
    this.connectionProxy = connectionProxy;
  }

  @esp.observeEvent(EventTypes.OPEN_CREATE_ORDER_MODAL)
  onOpenCreateOrderModal(event) {
    this.showModal = true;

    this.symbol = event.quote.symbol;
    this.buy = event.quote.buy;
    this.sell = event.quote.sell;
  }

  @esp.observeEvent(EventTypes.CLOSE_CREATE_ORDER_MODAL)
  onCloseCreateOrderModal() {
    this.showModal = false;
  }

  @esp.observeEvent(EventTypes.CREATE_ORDER)
  onCreateOrder(event) {
    try {
      this.connectionProxy.session.call("executeTrade", [{
        AssetPairId: event.symbol,
        Volume: event.volume,
        DefinedPrice: event.price,
        AccountId: user.id,
        TransactioAccountId: user.id
      }]);
    } catch (error) {
      _logger.error("Error placing order", error);
    }
    this.onCloseCreateOrderModal();
  }
}

export default CreateOrderViewModel;
