import * as esp from 'esp-js';
import { ViewModelBase } from '../../base/model/index';
import { EventTypes } from '../../../services/events/index';
import { OrderViewModel } from '../model/index';

/**
 * ViewModel for active orders
 */
class ActiveOrdersViewModel extends ViewModelBase {
  orders: OrderViewModel[]

  constructor(router: esp.Router) {
    super('activeOrdersViewModelId', router);
    this.orders = [];
  }

  @esp.observeEvent(EventTypes.ACTIVE_ORDERS_UPDATED)
  onUpdateActiveOrders(event) {
    this.orders = [];
    for (let orderDto of event.orders) {
      this.orders.push(
        OrderViewModel.fromDto(orderDto)
      );
    }
  }

}

export default ActiveOrdersViewModel;
