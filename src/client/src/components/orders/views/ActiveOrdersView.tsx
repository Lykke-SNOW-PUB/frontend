import * as React from 'react';
import { IObservableViewProps } from '../../base/views/index';
import { ActiveOrdersViewModel } from '../model/index';

import './activeOrders.css';

class ActiveOrdersView extends React.Component<IObservableViewProps<ActiveOrdersViewModel>, {}> {
  render() {
    let model = this.props.model;

    let upOrDown = Math.random() > .5;
    let cssClass = upOrDown ? "order_up" : "order_down";

    return (
      <table className="table orders-table">
        <thead>
          <tr>
            <th className="orders-table__cell_left-aligned">Instrument</th>
            <th className="orders-table__heading">B/S</th>
            <th className="orders-table__heading">Quantity</th>
            <th className="orders-table__heading">Order type</th>
            <th className="orders-table__heading">Price</th>
            <th className="orders-table__heading">Created on</th>
          </tr>
        </thead>
        <tbody>
          {model.orders.map(o => (
            <tr key={o.id}>
              <td className="orders-table__data orders-table__cell_left-aligned">{o.symbol}</td>
              <td className="orders-table__data order_up">{o.direction}</td>
              <td className="orders-table__data order_up">{o.quantity.toFixed(0)}</td>
              <td className="orders-table__data">{o.orderType}</td>
              <td className="orders-table__data">{o.definedPrice.toFixed(2)}</td>
              <td className="orders-table__data">{o.createdAt}</td>
            </tr>
          ))}
        </tbody>
      </table>
    )
  }
}

export default ActiveOrdersView;
