import * as React from 'react';
import {Side} from '../../../core/model/index';
import { IObservableViewProps } from '../../base/views/index';
import { OrderBookViewModel } from '../model/index';
import { FakeIdGen } from '../../../services/utils';

import './orderBook.css';

class OrderBookView extends React.Component<IObservableViewProps<OrderBookViewModel>, {}> {
  render() {
    let orderBook = this.props.model;

    return (
      <div className="order-book">
        {orderBook.items.map(i => (
          <div key={i.Symbol} className="order-book__item col-md-4">
            <div className="order-book__asset text-center">{i.Symbol}</div>
            <table className="order-book__prices table">
              <tbody>
                {i.BuyOrders.map(p => (
                  <tr key={FakeIdGen.next(i.Symbol)} className={"order-book__position_" + Side.Buy.name.toLowerCase()}>
                    <td className="order-book__cell">{Side.Buy.name}</td>
                    <td className="order-book__cell">{p.Volume.toFixed(0)}</td>
                    <td className="order-book__cell">{p.Price.toFixed(3)}</td>
                  </tr>
                ))}
                {i.SellOrders.map(p => (
                  <tr key={FakeIdGen.next(i.Symbol)} className={"order-book__position_" + Side.Sell.name.toLowerCase()}>
                    <td className="order-book__cell">{Side.Sell.name}</td>
                    <td className="order-book__cell">{p.Volume.toFixed(0)}</td>
                    <td className="order-book__cell">{p.Price.toFixed(3)}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        ))}
      </div>
    );
  }
}

export default OrderBookView;
