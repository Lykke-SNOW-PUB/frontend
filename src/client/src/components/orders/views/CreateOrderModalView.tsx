import * as React from 'react';
import * as esp from 'esp-js';
import { EventTypes } from '../../../services/events/index';
import { IObservableViewProps } from '../../base/views/index';
import { CreateOrderViewModel } from '../model/index';
import { Modal, Button, Tabs, Tab, Row, Col, Navbar, Nav, NavItem, Grid } from 'react-bootstrap';

import './createOrderModal.css';

interface ICreateOrderProps {
  show: boolean;
  onHide: any;
  model: CreateOrderViewModel;
  router: esp.Router;
}

interface ICreateOrderState {
  direction: string;
  orderType: string;
  volume: number;
  price: number;
}

class CreateOrderModalView extends React.Component<ICreateOrderProps, ICreateOrderState> {
  constructor() {
    super();
    this.state = {
      direction: "Sell",
      orderType: "Market",
      volume: 0,
      price: 0
    }
    this.handleCreateOrderClick = this.handleCreateOrderClick.bind(this);
    this.handleSelectDirection = this.handleSelectDirection.bind(this);
    this.handleSelectOrderType = this.handleSelectOrderType.bind(this);
    this.handleChangeVolume = this.handleChangeVolume.bind(this);
    this.handleChangePrice = this.handleChangePrice.bind(this);
  }

  handleCreateOrderClick(e: React.MouseEvent) {
    e.preventDefault();

    let selling = this.state.direction.toLowerCase() === "sell";

    this.props.router.publishEvent(
      'createOrderViewModelId',
      EventTypes.CREATE_ORDER,
      {
        symbol: this.props.model.symbol,
        volume: selling ? -this.state.volume : this.state.volume,
        price: this.state.price,
        orderType: this.state.orderType
      }
    );
  }

  handleSelectDirection(key) {
    this.setState({
      direction: key,
      orderType: this.state.orderType,
      volume: this.state.volume,
      price: this.state.price
    });
  }

  handleSelectOrderType(key) {
    this.setState({
      direction: this.state.direction,
      orderType: key,
      volume: this.state.volume,
      price: this.state.price
    });
  }

  handleChangeVolume(e) {
    this.setState({
      direction: this.state.direction,
      orderType: this.state.orderType,
      volume: e.target.value,
      price: this.state.price
    });
  }

  handleChangePrice(e) {
    this.setState({
      direction: this.state.direction,
      orderType: this.state.orderType,
      volume: this.state.volume,
      price: e.target.value
    });
  }

  render() {
    let createOrderModel = this.props.model;
    let router = this.props.router;

    if (createOrderModel.symbol == undefined || createOrderModel.symbol == '') {
      return null;
    }

    const modalProps = Object.assign({}, this.props);
    delete modalProps.router;
    delete modalProps.model;

    return (
      <Modal
        show={createOrderModel.showModal}
        onHide={() => {
          router.publishEvent('createOrderViewModelId', EventTypes.CLOSE_CREATE_ORDER_MODAL, {})
        } }
        {...modalProps}>
        <Modal.Header closeButton>
          <Modal.Title className="text-center">{createOrderModel.symbol.toUpperCase()}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="direction">
            <Tab.Container id="direction-tabs" defaultActiveKey="sell" onSelect={this.handleSelectDirection}>
              <Grid fluid>
                <Row className="clearfix">
                  <Col sm={6}>
                    <Nav>
                      <NavItem eventKey="sell">
                        SELL
                      </NavItem>
                    </Nav>
                  </Col>
                  <Col sm={6}>
                    <Nav pullRight>
                      <NavItem eventKey="buy">
                        BUY
                      </NavItem>
                    </Nav>
                  </Col>
                </Row>
                <Row>
                  <Tab.Content animation>
                    <Col sm={4}>
                      <Tab.Pane eventKey="sell">
                        <h4>{createOrderModel.sell}</h4>
                      </Tab.Pane>
                    </Col>
                    <Col sm={4}></Col>
                    <Col sm={4}>
                      <Tab.Pane eventKey="buy" >
                        <h4 className="direction-tabs__sell-tab">{createOrderModel.buy}</h4>
                      </Tab.Pane>
                    </Col>
                  </Tab.Content>
                </Row>
              </Grid>
            </Tab.Container>
          </div>
          <form className="form-horizontal">
            <div className="form-group">
              <label htmlFor="volume" className="control-label col-md-2 text-left">Volume:</label>
              <div className="col-md-6 pull-right">
                <input type="number" className="form-control" id="volume" onChange={this.handleChangeVolume} />
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="volume" className="control-label col-md-4 text-left">Trading volume:</label>
              <div className="col-md-6 pull-right text-right">
                <strong>1 168.65</strong> USD
                  </div>
            </div>
            <div className="form-group">
              <label htmlFor="volume" className="control-label col-md-2 text-left">Margin:</label>
              <div className="col-md-6 pull-right text-right">
                <strong>120</strong> EUR
                  </div>
            </div>

            <div className="order-type">
              <Tab.Container id="order-type-tabs" defaultActiveKey="market" onSelect={this.handleSelectOrderType}>
                <Grid fluid>
                  <Row className="clearfix">
                    <Col sm={12}>
                      <Nav bsStyle="tabs" justified>
                        <NavItem eventKey="market">
                          Market
                          </NavItem>
                        <NavItem eventKey="limit">
                          Limit
                          </NavItem>
                        <NavItem eventKey="stop">
                          Stop
                          </NavItem>
                      </Nav>
                      <Tab.Content animation>
                        <Tab.Pane eventKey="market">
                          <div className="form-group">
                            <label className="control-label col-md-4">If done order</label>
                            <div className="col-md-6 pull-right text-right">
                              <select className="form-control">
                                <option>None</option>
                              </select>
                            </div>
                          </div>
                          <div className="form-group">
                            <label className="control-label col-md-4">Take profit</label>
                            <div className="col-md-6 pull-right text-right">
                              <input type="number" className="form-control" />
                            </div>
                          </div>
                          <div className="form-group">
                            <label className="control-label col-md-4">Stop loss</label>
                            <div className="col-md-6 pull-right text-right">
                              <input type="number" className="form-control" />
                            </div>
                          </div>
                        </Tab.Pane>
                        <Tab.Pane eventKey="limit">
                          <div className="form-group">
                            <label htmlFor="price" className="control-label col-md-4">Price:</label>
                            <div className="col-md-6 pull-right text-right">
                              <input type="number" className="form-control" id="price" onChange={this.handleChangePrice} />
                            </div>
                          </div>
                          <hr />
                          <div className="form-group">
                            <label className="control-label col-md-4">If done order</label>
                            <div className="col-md-6 pull-right text-right">
                              <select className="form-control">
                                <option>None</option>
                              </select>
                            </div>
                          </div>
                          <div className="form-group">
                            <label className="control-label col-md-4">Take profit</label>
                            <div className="col-md-6 pull-right text-right">
                              <input type="number" className="form-control" />
                            </div>
                          </div>
                          <div className="form-group">
                            <label className="control-label col-md-4">Stop loss</label>
                            <div className="col-md-6 pull-right text-right">
                              <input type="number" className="form-control" />
                            </div>
                          </div>
                        </Tab.Pane>
                        <Tab.Pane eventKey="stop">
                          <div className="form-group">
                            <label className="control-label col-md-4">If done order</label>
                            <div className="col-md-6 pull-right text-right">
                              <select className="form-control">
                                <option>None</option>
                              </select>
                            </div>
                          </div>
                          <div className="form-group">
                            <label className="control-label col-md-4">Take profit</label>
                            <div className="col-md-6 pull-right text-right">
                              <input type="number" className="form-control" />
                            </div>
                          </div>
                          <div className="form-group">
                            <label className="control-label col-md-4">Stop loss</label>
                            <div className="col-md-6 pull-right text-right">
                              <input type="number" className="form-control" />
                            </div>
                          </div>
                        </Tab.Pane>
                      </Tab.Content>
                    </Col>
                  </Row>
                </Grid>
              </Tab.Container>
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button className="btn btn-modal btn-sell" onClick={this.handleCreateOrderClick}>CREATE ORDER</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default CreateOrderModalView;
