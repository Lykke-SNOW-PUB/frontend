import * as React from 'react';
import { SmartComponent } from 'esp-js-react';
import { InstrumentsView } from '../instruments/views/index';
import { ActiveOrdersView, OrderBookView } from '../orders/views/index';
import { ActivePositionsView } from '../positions/views/index';

const EmptyView = () => (
  <div>No corresponding view</div>
);

const ViewResolver: React.StatelessComponent<{ name: string }> = ({name}) => {
  // todo: refactor to fabric
  switch (name.trim().toLowerCase()) {
    case 'instruments':
      return <SmartComponent modelId="instrumentsViewModelId" view={InstrumentsView} />;
    case 'open orders':
      return <SmartComponent modelId="activeOrdersViewModelId" view={ActiveOrdersView} />;
    case 'order book':
      return <SmartComponent modelId="orderBookViewModelId" view={OrderBookView} />;
    case 'active positions':
      return <SmartComponent modelId="activePositionsViewModelId" view={ActivePositionsView} />;

    default:
      return <EmptyView />;
  }
}

export default ViewResolver;
