import * as React from 'react';
const logo = require('../../../public/Logo.svg');

export default () => <img src={logo} />
