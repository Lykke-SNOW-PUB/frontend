import * as React from 'react';
const avatar = require('../../../public/icons/avatar.svg');

export default () =>
  <div id="avatar" className="noselect">
    <img src={avatar} className="avatar" />
  </div>;
