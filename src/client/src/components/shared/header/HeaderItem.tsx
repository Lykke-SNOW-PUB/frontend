import * as React from 'react';

export interface IHeaderItemProps { title: string; }

export default class HeaderItem extends React.Component<IHeaderItemProps, {}> {
  render() {
    return <div className="mnuItem noselect">
      <img src={require('../../../../public/icons/' + this.props.title + '.svg')} className="mnuIcon" />
      <div>{this.props.title} <span className="mnuDropDown glyphicon glyphicon-menu-down" aria-hidden="true"></span></div>
    </div>
  }
}
