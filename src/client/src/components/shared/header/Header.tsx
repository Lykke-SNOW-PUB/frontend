import * as React from 'react';
import { RouterProvider, SmartComponent } from 'esp-js-react';
import { HeaderItem } from './index';
import { Avatar, MenuSeparator, Logo } from '../index';
import { BalanceInfoView } from '../../balance/views/index';
import { BalanceInfoViewModel } from '../../balance/model/index';

import './Header.css';

export default () => (
  <div className="mnuWrapper">
    <table className="mnuTable">
      <tbody>
        <tr>
          <td>
            <Logo />
          </td>
          <td>
            <SmartComponent modelId="balanceInfoViewModelId" view={BalanceInfoView}></SmartComponent>
          </td>
          <td>
            <MenuSeparator />
          </td>
          <td>
            <HeaderItem title="Layouts" />
          </td>
          <td>
            <HeaderItem title="Instruments" />
          </td>
          <td>
            <MenuSeparator />
          </td>
          <td>
            <HeaderItem title="Messages" />
          </td>
          <td>
            <Avatar />
          </td>
        </tr>
      </tbody>
    </table>
  </div>
);
