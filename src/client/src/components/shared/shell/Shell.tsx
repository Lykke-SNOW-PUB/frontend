import * as React from 'react';
import { ShellFrame, ShellViewModel } from './index';
import { IObservableViewProps } from '../../base/views/index';
import { EventTypes } from '../../../services/events/index';

export default class Shell extends React.Component<IObservableViewProps<ShellViewModel>, {}> {
  componentDidMount() {
    const frameWrappers = document.getElementsByClassName('frame-wrapper');
    for (let idx in frameWrappers) {
      let frameWrapper: any = frameWrappers.item(parseInt(idx));
      frameWrapper.style.height = (window.innerHeight - document.getElementsByClassName('mnuWrapper')[0].scrollHeight) / 2 - 1 + "px";
    }
  }

  render() {
    let router = this.props.router;
    let model = this.props.model;

    return (
      <div className="container-fluid">
        <div className="row frame-wrapper">
          <div className="col-sm-6">
            <ShellFrame />
          </div>
          <div className="col-sm-6">
            <ShellFrame />
          </div>
        </div>
        <div className="row frame-wrapper">
          <div className="col-sm-12">
            <ShellFrame />
          </div>
        </div>
      </div>
    );
  }
}
