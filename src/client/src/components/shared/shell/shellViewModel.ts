import * as esp from 'esp-js';
import { ViewModelBase } from '../../base/model/index';
import { EventTypes } from '../../../services/events/index';

export default class ShellViewModel extends ViewModelBase {
  constructor(router: esp.Router) {
    super('shellViewModelId', router);
  }
}

