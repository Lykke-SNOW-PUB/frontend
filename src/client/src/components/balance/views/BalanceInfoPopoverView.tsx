import * as React from 'react';
import { Popover } from 'react-bootstrap';
import { SmartComponent } from 'esp-js-react';
import { IObservableViewProps } from '../../base/views/index';
import { BalanceInfoViewModel } from '../model/index';

import './balanceInfoPopover.css';

class BalanceInfoPopoverContent extends React.Component<IObservableViewProps<BalanceInfoViewModel>, {}> {
  render() {
    let balanceInfo = this.props.model;
    let router = this.props.router;

    return <div>
      <table>
        <tbody>
          <tr>
            <td className="blncValue">
              0.00
            </td>
            <td>
              <div className="blncSeparator"></div>
            </td>
            <td>
              P&L of the day (real)
            </td>
          </tr>
          <tr>
            <td className="blncValue">0.00</td>
            <td><div className="blncSeparator"></div></td>
            <td>Deposits</td>
          </tr>
          <tr>
            <td className="blncValue">0.00</td>
            <td><div className="blncSeparator"></div></td>
            <td>Withdrawals</td>
          </tr>
          <tr>
            <td className="blncValue">0.00</td>
            <td><div className="blncSeparator"></div></td>
            <td>Comissions</td>
          </tr>
          <tr>
            <td colSpan={3} className="blncVerSeparator"></td>
          </tr>

          <tr>
            <td className="blncValue blncTotal accBalance">
              {balanceInfo.balance}
            </td>
            <td><div className="blncSeparator blncSeparatorTl"></div></td>
            <td>
              <div><b>Account balance</b></div>
              <div className="grayedText">10.03.2016</div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  }
}

const BalanceInfoPopoverView = (
  <Popover id="balanceInfoPopover">
    <SmartComponent modelId="balanceInfoViewModelId" view={BalanceInfoPopoverContent}></SmartComponent>
  </Popover>
)

export default BalanceInfoPopoverView;
