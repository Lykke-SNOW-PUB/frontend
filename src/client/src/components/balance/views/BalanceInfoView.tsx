import * as React from 'react';
import { OverlayTrigger } from 'react-bootstrap';
import { IObservableViewProps } from '../../base/views/index';
import { BalanceInfoViewModel } from '../model/index';
import { BalanceInfoPopoverView } from './index';

export default class BalanceInfoView extends React.Component<IObservableViewProps<BalanceInfoViewModel>, {}> {
  render() {
    let balanceInfo = this.props.model;
    let router = this.props.router;

    return <table className="mnuTable"><tbody><tr><td>
      <OverlayTrigger trigger="click" placement="bottom" overlay={BalanceInfoPopoverView}>
        <div id="accBalanceMenu" className="mnuItem noselect">
          <div className="accBalance">{balanceInfo.balance}</div>
          <div>Account balance <span className="mnuDropDown glyphicon glyphicon-menu-down" aria-hidden="true"></span></div>
        </div>
      </OverlayTrigger>
    </td>
      <td>
        <div className="mnuItem noselect">
          <div id="unrealizedPl">
            <span>{balanceInfo.unrealizedPl}</span> / day
            </div>
          <div>Unrealized P&L</div>
        </div>
      </td>
      <td>
        <div className="mnuItem noselect">
          <div id="totalCapital">{balanceInfo.totalCapital}</div>
          <div>Total capital</div>
        </div>
      </td>
      <td>
        <div className="mnuItem noselect">
          <div id="margin">{balanceInfo.margin}</div>
          <div>Margin</div>
        </div>
      </td>
      <td>
        <div className="mnuItem noselect">
          <div id="marginPercent">{balanceInfo.marginPercent}%</div>
          <div>Margin %</div>
        </div>
      </td>
      <td>
        <div className="mnuItem noselect">
          <div id="freeCapital">{balanceInfo.freeCapital}</div>
          <div>Free capital</div>
        </div>
      </td></tr></tbody></table>
  }
}
