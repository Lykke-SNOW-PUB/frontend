import * as esp from 'esp-js';
import { ViewModelBase } from '../../base/model/index';
import { EventTypes } from '../../../services/events/index';
import { BrokerageAccount, BalanceInfo, getProfitLoss } from '../../../core/model/index';

export default class BalanceInfoViewModel extends ViewModelBase {
  balance: string;
  unrealizedPl: string;
  totalCapital: string;
  margin: string;
  marginPercent: string;
  freeCapital: string;

  constructor(router: esp.Router) {
    super('balanceInfoViewModelId', router);
    this.balance =
      this.unrealizedPl =
      this.totalCapital =
      this.margin =
      this.marginPercent =
      this.freeCapital = '...';
  }

  @esp.observeEvent(EventTypes.UPDATE_BALANCE)
  private onUpdateBalance(event) {
    let updatedBalanceInfo = new BalanceInfo(event.balance);

    this.balance = updatedBalanceInfo.balance.toFixed(2);
    this.unrealizedPl = updatedBalanceInfo.unrealizedPl.toFixed(2);
    this.totalCapital = updatedBalanceInfo.totalCapital.toFixed(2);
    this.margin = updatedBalanceInfo.margin.toFixed(2);
    this.marginPercent = updatedBalanceInfo.marginPercent.toFixed(2);
    this.freeCapital = updatedBalanceInfo.freeCapital.toFixed(2);
  }
}
