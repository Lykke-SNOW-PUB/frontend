import * as esp from 'esp-js';

interface IObservableViewProps<TModel> {
  model: TModel;
  router: esp.Router;
}

export default IObservableViewProps;
