import * as esp from 'esp-js';

export default class ViewModelBase {
  constructor(public modelId: string, public router: esp.Router) {
  }

  observeEvents() {
    this.router.addModel(this.modelId, this);
    this.router.observeEventsOn(this.modelId, this);
  }

  dispose() {
    this.router.removeModel(this.modelId);
  }
}
