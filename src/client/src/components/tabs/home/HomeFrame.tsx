import * as React from 'react';
import { HomeFrameItem } from './index';
import './frames.css';
import '../../../styles/scrollbars.css';

import { Popover, OverlayTrigger } from 'react-bootstrap';

export interface IHomeFrameProps {
  onChangeTabItem: any;
}

export default class HomeFrame extends React.Component<IHomeFrameProps, {}> {

  constructor(props: IHomeFrameProps) {
    super(props);
  }

  public changeTabItemHandler(label: string, content: any) {
    if (typeof this.props.onChangeTabItem === 'function') {
      this.props.onChangeTabItem(label, content);
    }
  }

  render() {
    return (
      <div>
        <HomeFrameItem title="Trades" onChangeTabItem={(label: string, content: any) => this.changeTabItemHandler(label, content)} />
        <HomeFrameItem title="Orders" onChangeTabItem={(label: string, content: any) => this.changeTabItemHandler(label, content)} />
        <HomeFrameItem title="Watchlist" onChangeTabItem={(label: string, content: any) => this.changeTabItemHandler(label, content)} />
        <HomeFrameItem title="Dealings" onChangeTabItem={(label: string, content: any) => this.changeTabItemHandler(label, content)} />
        <HomeFrameItem title="Activities" onChangeTabItem={(label: string, content: any) => this.changeTabItemHandler(label, content)} />
        <HomeFrameItem title="Charts" onChangeTabItem={(label: string, content: any) => this.changeTabItemHandler(label, content)} />
        <HomeFrameItem title="News" onChangeTabItem={(label: string, content: any) => this.changeTabItemHandler(label, content)} />
      </div>
    );
  }
}
