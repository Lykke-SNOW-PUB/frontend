import * as React from 'react';
import { OverlayTrigger } from 'react-bootstrap';
import { HomeFrameItemPopover } from './index';
import { ViewResolver } from '../../shared/index';

export interface IHomeFrameItemProps {
  title: string;
  onChangeTabItem: any;
}

export default class HomeFrameItem extends React.Component<IHomeFrameItemProps, {}> {
  constructor(props: IHomeFrameItemProps) {
    super(props);
    this.frameItemClicked = this.frameItemClicked.bind(this);
    this.popoverItemClickHandler = this.popoverItemClickHandler.bind(this);
  }

  frameItemClicked(event: React.MouseEvent) {
    if (typeof this.props.onChangeTabItem === 'function') {
      //this.props.onChangeTabItem(this.props.title, AssetPairs);
    }
  }

  popoverItemClickHandler(popoverItem: string) {
    this.props.onChangeTabItem(popoverItem, <ViewResolver name={popoverItem} />);
  }

  render() {
    return (
      <OverlayTrigger
        trigger="click" rootClose
        placement="bottom"
        overlay={
          <HomeFrameItemPopover
            id={"popover-itemType-" + this.props.title}
            itemType={this.props.title}
            onPopoverItemClicked={this.popoverItemClickHandler} />
        }>
        <div className="item" onClick={this.frameItemClicked}>
          <img src={require('../../../../public/icons/' + this.props.title + '.svg')} className="icon" />
          <div className="caption">
            <a href="#" role="tab" data-toggle="tab">{this.props.title}</a>
          </div>
        </div>
      </OverlayTrigger>
    );
  }
}
