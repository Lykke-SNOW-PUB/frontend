import * as React from 'react';
import { Popover } from 'react-bootstrap';
import '../../../styles/popovers.css';

class HomeFrameItemPopover extends React.Component<{ id: string, itemType: string, onPopoverItemClicked: any }, {}> {
  constructor() {
    super();
    this.popoverItemClicked = this.popoverItemClicked.bind(this);
  }

  items = (itemType: string): string[] => {
    switch (itemType.toLowerCase()) {
      case 'trades':
        return ['Instruments', 'Active positions'];
      case 'orders':
        return ['Open orders', 'Closed orders', 'Order book']
      default:
        return [];
    }
  }

  popoverItemClicked(item: string) {
    this.props.onPopoverItemClicked(item);
  }

  render() {
    const items = this.items(this.props.itemType);
    if (items.length == 0) {
      return null;
    }

    const popoverProps = Object.assign({}, this.props);
    delete popoverProps.onPopoverItemClicked;

    return (
      <Popover {...popoverProps}>
        <ul>
          {items.map((item) => {
            return (<li key={item} onClick={() => this.popoverItemClicked(item)}><a href="#">{item}</a></li>)
          })}
        </ul>
      </Popover>
    );
  }
}

export default HomeFrameItemPopover;
