import * as React from 'react';
import { HomeFrame } from './home/index';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

export interface ITabContainerState {
  selectedIndex: number;
  tabs: {
    label: string,
    content: any
  }[];
}

export default class TabContainer extends React.Component<{}, ITabContainerState> {

  constructor() {
    super();
    Tabs.setUseDefaultStyles(false);
    this.state = {
      selectedIndex: 0,
      tabs: [
        { label: 'Home 1', content: <HomeFrame onChangeTabItem={(label: string, content: any) => this.changeTab(label, content)} /> },
        { label: 'Home 2', content: <HomeFrame onChangeTabItem={(label: string, content: any) => this.changeTab(label, content)} /> },
        { label: 'Home 3', content: <HomeFrame onChangeTabItem={(label: string, content: any) => this.changeTab(label, content)} /> },
      ],
    }
  }

  public addTab(label: string, content: any) {
    const defaultLabel = "Home";
    const defaultContent = HomeFrame;

    if (label === "") {
      label = defaultLabel;
    }

    if (!content) {
      content = defaultContent;
    }

    this.setState({
      tabs: [
        ...this.state.tabs,
        { label, content },
      ],
      selectedIndex: this.state.tabs.length,
    });
  }

  public removeTab(index: number) {
    this.setState({
      tabs: this.state.tabs.filter((tab, i) => i !== index),
      selectedIndex: this.state.selectedIndex - 1,
    });
  }

  public changeTab(label: string, content: any) {
    if (label === "" || !content) {
      return;
    }

    var tab = this.getCurrentTab();
    tab.label = label;
    tab.content = content;

    this.setState({
      tabs: this.state.tabs,
      selectedIndex: this.state.selectedIndex,
    });
  }

  public getCurrentTab() {
    return this.state.tabs[this.state.selectedIndex];
  }

  render() {
    return (
      <div>
        <Tabs selectedIndex={this.state.selectedIndex}
          onSelect={selectedIndex => this.setState({ selectedIndex } as ITabContainerState)}>
          <TabList className="homeTabs">
            {this.state.tabs.map((tab, i) => (
              <Tab className="item tab" key={i}>
                <div className="caption">
                  {tab.label}
                  <span className="closeIcon glyphicon glyphicon-remove-sign"
                    aria-hidden="true" onClick={() => this.removeTab(i)}></span>
                </div>
              </Tab>
            ))}
            <div className="item mnu addTab"
              onClick={() =>
                this.addTab("Home",
                  <HomeFrame onChangeTabItem={(label: string, content: any) => this.changeTab(label, content)} />)}>
              <div className="mnuIcon"><span className="glyphicon glyphicon-plus" aria-hidden="true"></span></div>
            </div>
          </TabList>

          {this.state.tabs.map((tab, i) =>
            <TabPanel key={i} className="tab-pane">
              {tab.content}
            </TabPanel>
          )}
        </Tabs>
      </div>
    );
  }
}
