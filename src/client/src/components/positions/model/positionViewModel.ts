class PositionViewModel {
  id: string;
  instrument: string;
  sell: number;
  buy: number;
  LS: number;
  quantity: number;
  takeProfit: number;
  stopLoss: number;
  tradeId: string;

  mapFromDto(positionDto: any) {
    this.id = positionDto.id;
    this.instrument = positionDto.instrument;
    this.sell = positionDto.sell.toFixed(3);
    this.buy = positionDto.buy.toFixed(3);
    this.LS = positionDto.LS.toFixed(2);
    this.quantity = positionDto.quantity.toFixed(0);
    this.takeProfit = positionDto.takeProfit.toFixed(2);
    this.stopLoss = positionDto.stopLoss.toFixed(2);
    this.tradeId = positionDto.tradeId;

    return this;
  }
}

export default PositionViewModel;
