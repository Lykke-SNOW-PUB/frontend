import * as esp from 'esp-js';
import { ViewModelBase } from '../../base/model/index';
import { EventTypes } from '../../../services/events/index';
import { IObservableConnection, IConnectionProxy, AutobahnConnectionProxy } from '../../../system/index';
import { PositionViewModel } from '../model/index';

/**
 * ViewModel for active positions
 */
class ActivePositionsViewModel extends ViewModelBase {
  positions: PositionViewModel[];

  constructor(router: esp.Router, private connectionProxy?: IConnectionProxy) {
    super('activePositionsViewModelId', router);
    this.positions = [];
  }

  @esp.observeEvent(EventTypes.POSITIONS_UPDATED)
  onPositionsUpdated(event) {
    this.positions = [];
    for (let positionDto of event.positions) {
      this.positions.push(
        new PositionViewModel().mapFromDto(positionDto)
      );
    }
  }

  @esp.observeEvent(EventTypes.CLOSE_POSITION)
  onClosePosition(event) {
    try {
      this.connectionProxy.session
        .call('CloseOrder', [{ orderId: event.id }])
    } catch (error) {
      console.log(error);
    }
  }
}

export default ActivePositionsViewModel;
