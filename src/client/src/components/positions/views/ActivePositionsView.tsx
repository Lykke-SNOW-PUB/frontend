import * as React from 'react';
import { IObservableViewProps } from '../../base/views/index';
import { EventTypes } from '../../../services/events/index';
import { ActivePositionsViewModel } from '../model/index';

import './activePositions.css';

class ActivePositionsView extends React.Component<IObservableViewProps<ActivePositionsViewModel>, {}> {
  render() {
    let positionsModel = this.props.model;
    let router = this.props.router;

    return (
      <div>
        <table className="table positions-table">
          <thead>
            <tr>
              <th className="positions-table__th positions-table__th_left-aligned">Instrument</th>
              <th className="positions-table__th">Sell</th>
              <th className="positions-table__th">Buy</th>
              <th className="positions-table__th">L/S</th>
              <th className="positions-table__th">Quantity</th>
              <th className="positions-table__th">Take profit</th>
              <th className="positions-table__th">Stop loss</th>
              <th className="positions-table__th">Trade ID</th>
              <th className="positions-table__th">Close</th>
            </tr>
          </thead>
          <tbody>
            {positionsModel.positions.map(p => (
              <tr key={p.id + p.instrument}>
                <td className="positions-table__td positions-table__td_left-aligned">{p.instrument}</td>
                <td className="positions-table__td">{p.sell}</td>
                <td className="positions-table__td">{p.buy}</td>
                <td className="positions-table__td">{p.LS}</td>
                <td className="positions-table__td">{p.quantity}</td>
                <td className="positions-table__td">{p.takeProfit}</td>
                <td className="positions-table__td">{p.stopLoss}</td>
                <td className="positions-table__td">{p.tradeId}</td>
                <td className="positions-table__td">
                  <button
                    className="btn btn-trade"
                    onClick={() => router.publishEvent('activePositionsViewModelId', EventTypes.CLOSE_POSITION, { id: p.id })}>
                    Close
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

export default ActivePositionsView;
