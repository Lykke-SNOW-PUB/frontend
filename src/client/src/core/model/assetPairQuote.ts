﻿class AssetPairQuote {
  ask: number;
  bid: number;
  symbol: string;
  timestamp: Date;
}

export default AssetPairQuote;
