class Side {
  private static _buy = new Side('Buy');
  private static _sell = new Side('Sell');

  name: string;

  static get Buy() {
    return this._buy;
  }

  static get Sell() {
    return this._sell;
  }

  constructor(name: string) {
    this.name = name;
  }
}

export default Side;
