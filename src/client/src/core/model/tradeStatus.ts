class TradeStatus {
  private static _pending = new TradeStatus('Pending');
  private static _done = new TradeStatus('Done');
  private static _rejected = new TradeStatus('Rejected');

  name: string;

  static get Pending() {
    return this._pending;
  }

  static get Done() {
    return this._done;
  }

  static get Rejected() {
    return this._rejected;
  }

  constructor(name: string) {
    this.name = name;
  }
}

export default TradeStatus;
