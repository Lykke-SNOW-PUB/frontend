﻿class AssetPair {
  constructor(public baseAssetId: string, public quoteAssetId: string) {
  }

  accuracy: number;
  invertedAccuracy: number;
  symbol: string;
}

export default AssetPair;
