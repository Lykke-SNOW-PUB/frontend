class PriceMovementType {
  private static _none = new PriceMovementType('None');
  private static _up = new PriceMovementType('Up');
  private static _down = new PriceMovementType('Down');

  name: string;

  static get None() {
    return this._none;
  }

  static get Up() {
    return this._up;
  }

  static get Down() {
    return this._down;
  }

  constructor(name: string) {
    this.name = name;
  }
}

export default PriceMovementType;
