class HistoricPosition {
  _timestamp: Date;
  _eurPnl: number;

  constructor(timestamp: Date, eurPnl: number) {
    this._timestamp = timestamp;
    this._eurPnl = eurPnl;
  }

  get timestamp(): Date {
    return this._timestamp;
  }

  get eurPnl(): number {
    return this._eurPnl;
  }
}

export default HistoricPosition;
