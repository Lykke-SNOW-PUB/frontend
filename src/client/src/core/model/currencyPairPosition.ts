import { CurrencyPair } from './';

class CurrencyPairPosition {
  _symbol: string;
  _currencyPair: CurrencyPair;
  _basePnl: number;
  _baseTradedAmount: number;

  constructor(symbol: string, basePnl: number, baseTradedAmount: number, currencyPair: CurrencyPair) {
    this._symbol = symbol;
    this._currencyPair = currencyPair;
    this._basePnl = basePnl;
    this._baseTradedAmount = baseTradedAmount;
  }

  get symbol(): string {
    return this._symbol;
  }

  get currencyPair(): CurrencyPair {
    return this._currencyPair;
  }

  get basePnl(): number {
    return this._basePnl;
  }

  get baseTradedAmount(): number {
    return this._baseTradedAmount;
  }
}

export default CurrencyPairPosition;
