import { Trade } from './';

export default class TradesUpdate {

  constructor(private _isStateOfTheWorld: boolean, private _isStale: boolean, private _trades: Array<Trade>) {
  }

  get isStateOfTheWorld(): boolean {
    return this._isStateOfTheWorld;
  }
  get isStale(): boolean {
    return this._isStale;
  }

  get trades(): Array<Trade> {
    return this._trades;
  }
}
