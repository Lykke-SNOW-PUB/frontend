import { BrokerageAccount, getProfitLoss } from './index';

export default class BalanceInfo {
  balance: number;
  unrealizedPl: number;
  totalCapital: number;
  margin: number;
  marginPercent: number;
  freeCapital: number;

  constructor(newBalance: number) {
    this.recalculateMetrics(newBalance);
  }

  private recalculateMetrics(newBalance: number): void {
    this.balance = newBalance;
    this.unrealizedPl = getProfitLoss();
    this.totalCapital = this.balance * BrokerageAccount.leverage + this.unrealizedPl;
    this.margin = this.totalCapital - this.balance;
    this.marginPercent = this.margin / this.totalCapital;
    this.freeCapital = this.totalCapital - (this.balance + this.unrealizedPl);
  }
}
