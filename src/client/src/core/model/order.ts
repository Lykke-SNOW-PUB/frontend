﻿import { Direction } from './index';

class Order {
  constructor(
    public assetPairId: string,
    public volume: number,
    public direction: Direction) {
  }

  id: string;
  price: number;
}

export default Order;
