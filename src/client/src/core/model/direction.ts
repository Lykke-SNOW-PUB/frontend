﻿export enum Direction {
    Buy,
    Sell
}

export default Direction;
