class MockAccountInfo {
  static defaultAccountBalance = 50000;

  static getBalance() {
    return this.defaultAccountBalance + Math.random()*1000;
  }
}

export default MockAccountInfo;
