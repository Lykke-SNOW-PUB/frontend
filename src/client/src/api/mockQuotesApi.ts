import { mockAssetPairs } from './index';

class MockQuotesApi {
  private static quoteUpdates = [];

  static getQuoteUpdates() {
    this.quoteUpdates = [];
    mockAssetPairs.map(quote => {
      this.quoteUpdates = [{
        symbol: quote,
        buy: Math.random() * 1000,
        sell: Math.random() * 1000,
        change: Math.random() * 10,
        changeRate: Math.random()
      }, ...this.quoteUpdates]
    })

    return this.quoteUpdates;
  }
}

export default MockQuotesApi;
