import { mockAssetPairs } from './index';
import { FakeIdGen } from '../services/utils';

class MockPositionsApi {
  private static activePositions = [];

  static getActivePositions() {
    this.activePositions = [];
    mockAssetPairs.map(asset => {
      let idx: number;
      this.activePositions = [{
        id: FakeIdGen.next('POS'),
        instrument: asset,
        sell: Math.random(),
        buy: Math.random(),
        LS: Math.random(),
        quantity: Math.random(),
        takeProfit: Math.random(),
        stopLoss: Math.random(),
        tradeId: FakeIdGen.next('BNP')
      }, ...this.activePositions]
    })

    return this.activePositions;
  }
}

export default MockPositionsApi;
