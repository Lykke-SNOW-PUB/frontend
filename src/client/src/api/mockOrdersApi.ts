import { mockAssetPairs } from './index';
import { FakeIdGen } from '../services/utils';

class MockOrdersApi {
  private static activeOrders = [];

  static getActiveOrders() {
    this.activeOrders = [];
    mockAssetPairs.map(assetPair => {
      this.activeOrders = [{
        id: FakeIdGen.next('ORD'),
        assetPairId: assetPair,
        volume: Math.random() * 1000,
        definedPrice: Math.random() * 100
      }, ...this.activeOrders]
    });

    return this.activeOrders;
  }

  static getOrderBook(marketCapacity = 4) {
    let orderBookItems = [];
    for (let symbol of mockAssetPairs) {
      let orderBookItemPositions = [];
      for (var i = 0; i < marketCapacity; i++) {
        orderBookItemPositions = [{
          direction: i > marketCapacity / 2 - 1 ? 'Buy' : 'Sell',
          volume: Math.random() * 1000,
          price: Math.random() * 100
        }, ...orderBookItemPositions]
      }
      orderBookItems = [{
        symbol: symbol,
        positions: orderBookItemPositions
      }, ...orderBookItems]
    }

    return orderBookItems;
  }
}

export default MockOrdersApi;
