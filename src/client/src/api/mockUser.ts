import { User } from '../core/model/index';

export function mockUser(): User {
  let user = new User('42', '42@42.com', '42 42 42');
  user.id = '42';

  return user;
}
