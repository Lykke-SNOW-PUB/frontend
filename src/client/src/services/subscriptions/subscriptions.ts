export default {
  UPDATE_BALANCE: 'AccountUpdated',
  UPDATE_QUOTES: "AssetPairPriceUpdated",
  UPDATE_ACTIVE_ORDERS: "ActiveOrdersUpdated"
}
