class FakeIdGen {
  private static idx: number = 0;

  static next(prefix: string) {
    return `${prefix}-${this.idx++}`;
  }
}

export { FakeIdGen };
