import * as React from 'react';
import { render } from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import App from './components/App';
import AppBootstrapper from './appBootstrapper';

const { AppContainer } = require('react-hot-loader');

// Tell Typescript there is a global variable called module
declare var module: { hot: any };

const app = new AppBootstrapper();
app.run();

render(
  <AppContainer>
    <App />
  </AppContainer>,
  document.getElementById('root')
);

if (module.hot) {
  module.hot.accept('./components/App', () => {
    const NextApp = require('./components/App').default;
    render(
      <AppContainer>
        <NextApp />
      </AppContainer>,
      document.getElementById('root')
    );
  });
}
