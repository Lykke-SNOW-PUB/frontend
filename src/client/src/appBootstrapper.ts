import * as esp from 'esp-js';

import { ShellViewModel } from './components/shared/shell/index';
import { BalanceInfoViewModel } from './components/balance/model/index';
import { InstrumentsViewModel } from './components/instruments/model/index';
import { ActiveOrdersViewModel, OrderBookViewModel, CreateOrderViewModel } from './components/orders/model/index';
import { ActivePositionsViewModel } from './components/positions/model/index';

import {
  IConnectionProxy, Connection, SignalRConnectionProxy, AutobahnConnectionProxy,
  router, logger
} from './system/index';

import { EventTypes } from './services/events/index';
import { Subscriptions } from './services/subscriptions/index';

import { MockQuotesApi, MockAccountInfoApi, MockPositionsApi, MockOrdersApi, mockUser } from './api/index';

const config = require('config.json');
const _logger = logger.create('AppBootstrapper');

const user = mockUser();

export default class AppBootstrapper {
  private balanceInfoViewModelId: string = "balanceInfoViewModelId";
  private connectionProxy: IConnectionProxy;

  run(): void {
    this.startServices();
    this.startModels();
    this.startApi();
  }

  balanceUpdateHandler = (args, kwargs) => {
    router.publishEvent(this.balanceInfoViewModelId, EventTypes.UPDATE_BALANCE, {
      balance: (args[0] && args[0].Balance) | kwargs.Balance
    });
  }

  quoteUpdateHandler = (args, kwargs) => {
    router.publishEvent('instrumentsViewModelId', EventTypes.UPDATE_QUOTE, {
      quote: args
    });
  }

  initQuotesHandler = (quotes) => {
    router.publishEvent('instrumentsViewModelId', EventTypes.INIT_QUOTES, {
      quotes: quotes
    });
  }

  orderBookUpdateHandler = (orderBook) => {
    router.publishEvent('orderBookViewModelId', EventTypes.ORDER_BOOK_UPDATED, {
      items: orderBook
    });
  }

  activePositionsUpdateHandler = (orders) => {
    _logger.info(orders);
    router.publishEvent('activeOrdersViewModelId', EventTypes.ACTIVE_ORDERS_UPDATED, {
      orders: orders
    });
  }

  startServices(): void {
    _logger.info('Starting app');
    this.connectionProxy = new AutobahnConnectionProxy(config.serverEndpointUrl, "realm1");
    this.connectionProxy.onOpen((session, details) => {
      this.connectionProxy.session.call("getMarketProfile").then(
        (quotes) => {
          this.initQuotesHandler(quotes);
        });
      this.connectionProxy.session.call("getAccountInfo", [user.id]).then(
        (accountinfo) => this.balanceUpdateHandler([], accountinfo)
      );
      this.connectionProxy.session.call("getOrderBook").then(
        (orderBook) => this.orderBookUpdateHandler(orderBook)
      );
      this.connectionProxy.session.call("getOpenPositions", [user.id]).then(this.activePositionsUpdateHandler);

      this.connectionProxy.session.subscribe(Subscriptions.UPDATE_BALANCE, this.balanceUpdateHandler);
      this.connectionProxy.session.subscribe(Subscriptions.UPDATE_QUOTES, this.quoteUpdateHandler);
      this.connectionProxy.session.subscribe(Subscriptions.UPDATE_ACTIVE_ORDERS, (args) =>
        this.activePositionsUpdateHandler(args[0]));
    });

    const connection = new Connection(this.connectionProxy, {}, user.id)
      .open();
  }

  startModels(): void {
    let balanceInfoViewModel = new BalanceInfoViewModel(router);
    balanceInfoViewModel.observeEvents();

    let shellViewModel = new ShellViewModel(router);
    shellViewModel.observeEvents();

    let instrumentsViewModel = new InstrumentsViewModel(router);
    instrumentsViewModel.observeEvents();

    let activeOrdersViewModel = new ActiveOrdersViewModel(router);
    activeOrdersViewModel.observeEvents();

    let orderBookViewModel = new OrderBookViewModel(router);
    orderBookViewModel.observeEvents();

    let activePositionsViewModel = new ActivePositionsViewModel(router, this.connectionProxy);
    activePositionsViewModel.observeEvents();

    let createOrderViewModel = new CreateOrderViewModel(router, this.connectionProxy);
    createOrderViewModel.observeEvents();
  }

  startApi(): void {
    setInterval(() => {
      router.publishEvent('activePositionsViewModelId', EventTypes.POSITIONS_UPDATED, {
        positions: MockPositionsApi.getActivePositions()
      });
    }, 3000);
  }
}
