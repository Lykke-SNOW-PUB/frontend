const path = require('path');
const webpack = require('webpack');
const failPlugin = require('webpack-fail-plugin');
const WebpackNotifierPlugin = require('webpack-notifier');
const args = {
  endpoint: 'local'
};
const config = args.endpoint + '.config.json';

module.exports = {
  debug: false,
  devtool: 'source-map',
  entry: [
    'react-hot-loader/patch',
    path.resolve(__dirname, 'src/index'),
  ],
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/dist/',
    filename: 'index.js'
  },
  target: 'web',
  resolve: {
    extensions: ['', '.webpack.js', '.web.js', '.ts', '.tsx', '.js', 'jsx'],
    modulesDirectories: ['src', 'node_modules'],
    alias: {
      'config.json': path.resolve(__dirname, 'config', config)
    }
  },
  module: {
    preLoaders: [
      { test: /\.js$/, loader: 'source-map-loader' }
      //{ test: /\.tsx?$/, loader: 'tslint', exclude: ['node_modules'] }
    ],
    loaders: [
      { test: /\.css$/, loader: 'style!css' },
      { test: /\.tsx?$/, loaders: ['babel', 'ts'] },
      { test: /jquery\.js$/, loader: 'expose?$!expose?jQuery' }, // make $ and jQuery available globally (as a window prop) because of SignalR rely on it
      {
        test: /\.json$/,
        loader: 'json-loader'
      },
      { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file' },
      { test: /\.(woff|woff2)$/, loader: 'url?prefix=font/&limit=5000' },
      { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/octet-stream' },
      { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=image/svg+xml' }
    ],
    noParse: ["ws"]
  },
  externals: ["ws"],
  plugins: [
    new webpack.NoErrorsPlugin(),
    failPlugin,
    new WebpackNotifierPlugin({ alwaysNotify: true }),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: true
      }
    })
  ]
}
