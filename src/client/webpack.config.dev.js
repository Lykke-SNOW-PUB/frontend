const path = require('path');
const webpack = require('webpack');
const failPlugin = require('webpack-fail-plugin');
const WebpackNotifierPlugin = require('webpack-notifier');
const processArgs = process.argv.slice(2);
const config = processArgs[processArgs.length-1] + '.config.json';

module.exports = {
  debug: true,
  devtool: 'cheap-module-eval-source-map',
  entry: [
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://localhost:3000',
    'webpack/hot/only-dev-server',
    path.resolve(__dirname, 'src/index')
  ],
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/',
    filename: 'index.js'
  },
  target: 'web',
  devServer: {
    port: 3000,
    contentBase: path.resolve(__dirname, 'src'),
    historyApiFallback: true,
    hot: true
  },
  resolve: {
    extensions: ['', '.webpack.js', '.web.js', '.ts', '.tsx', '.js', 'jsx'],
    modulesDirectories: ['src', 'node_modules'],
    alias: {
      'config.json': path.resolve(__dirname, 'config', config)
    }
  },
  module: {
    preLoaders: [
      { test: /\.js$/, loader: 'source-map-loader' }
      //{ test: /\.tsx?$/, loader: 'tslint', exclude: ['node_modules'] }
    ],
    loaders: [
      { test: /\.css$/, loader: 'style!css' },
      { test: /\.tsx?$/, loaders: ['babel', 'ts'] },
      { test: /jquery\.js$/, loader: 'expose?$!expose?jQuery' }, // make $ and jQuery available globally (as a window prop) because of SignalR rely on it
      {
        test: /\.json$/,
        loader: 'json-loader'
      },
      { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file' },
      { test: /\.(woff|woff2)$/, loader: 'url?prefix=font/&limit=5000' },
      { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/octet-stream' },
      { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=image/svg+xml' }
    ],
    noParse: ["ws"]
  },
  externals: ["ws"],
  plugins: [
    new webpack.HotModuleReplacementPlugin({
      multiStep: true // Enable multi-pass compilation for enhanced performance in larger projects
    }),
    new webpack.NoErrorsPlugin(),
    failPlugin,
    new WebpackNotifierPlugin({ alwaysNotify: true })
  ]
}
