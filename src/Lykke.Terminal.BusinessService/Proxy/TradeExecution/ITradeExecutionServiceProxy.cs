﻿using System.Threading.Tasks;
using Lykke.Terminal.Domain.ApplicationServices.Trading;

namespace Lykke.Terminal.BusinessService.Proxy.TradeExecution
{
    public interface ITradeExecutionServiceProxy
    {
        Task ExecuteTrade(OpenOrderContext order);
    }
}