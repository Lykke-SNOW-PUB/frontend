﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Lykke.Terminal.Common.Log;
using Lykke.Terminal.Domain.ApplicationServices.Trading;
using Lykke.Terminal.Domain.Exchange;
using Lykke.Terminal.Infrastructure.Concerns.RabbitMq;
using Lykke.Terminal.Messaging;
using Newtonsoft.Json;
using WampSharp.V2.Rpc;

namespace Lykke.Terminal.BusinessService.Proxy.TradeExecution
{
    public class TradeExecutionServiceProxy : ITradeExecutionServiceProxy
    {
        private readonly IHttpServiceProxy _httpServiceProxy;
        private readonly ILog _logger;
        private readonly IHttpServiceProxy _tradingWebServicesProxy;
        private readonly IBroker _broker;

        public TradeExecutionServiceProxy(IHttpServiceProxy httpServiceProxy, ILog logger, IHttpServiceProxy tradingWebServicesProxy, IBroker broker)
        {
            _httpServiceProxy = httpServiceProxy;
            _logger = logger;
            _tradingWebServicesProxy = tradingWebServicesProxy;
            _broker = broker;
        }

        //todo: expose wrapper and decorate it with wamp specific, whilst core logic will be implemented in ExecuteTradeImpl
        [WampProcedure("executeTrade")]
        public async Task ExecuteTrade(OpenOrderContext order)
        {
            var resp = await _httpServiceProxy.Post("order", order);
            await _logger.WriteInfo(this.ToString(), "TradeExecutionServiceProxy", JsonConvert.SerializeObject(order), resp.ToString(), DateTime.Now);

            if (resp.IsSuccessStatusCode)
            {
                var activeOrders = await _tradingWebServicesProxy.Get<IEnumerable<OrderBase>>($"activeorder/{order.AccountId}");
                _broker.PublishToTopic(KnownTopics.ActiveOrdersUpdated, activeOrders);
            }
        }
    }
}