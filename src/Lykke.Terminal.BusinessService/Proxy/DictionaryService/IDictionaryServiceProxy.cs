﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Lykke.Terminal.Domain.Assets;

namespace Lykke.Terminal.BusinessService.Proxy.DictionaryService
{
    public interface IDictionaryServiceProxy: IProxy
    {
        Task<IEnumerable<AssetPair>> GetAssetPairsAsync();
    }
}