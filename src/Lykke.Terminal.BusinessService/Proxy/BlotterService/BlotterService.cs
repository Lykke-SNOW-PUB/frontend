﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Lykke.Terminal.Domain.Exchange;
using WampSharp.V2.Rpc;

namespace Lykke.Terminal.BusinessService.Proxy.BlotterService
{
    public class BlotterService : IBlotterService
    {
        private readonly IHttpServiceProxy _serviceProxy;

        public BlotterService(IHttpServiceProxy serviceProxy)
        {
            _serviceProxy = serviceProxy;
        }

        [WampProcedure("getOpenPositions")]
        public async Task<IEnumerable<IOrderBase>> GetOpenPositions(string accountId)
        {
            return 
                await _serviceProxy.Get<IEnumerable<OrderBase>>($"activeorder/{accountId}");
        }
    }
}