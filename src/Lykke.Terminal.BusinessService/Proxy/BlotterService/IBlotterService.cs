﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Lykke.Terminal.Domain.Exchange;

namespace Lykke.Terminal.BusinessService.Proxy.BlotterService
{
    public interface IBlotterService
    {
        Task<IEnumerable<IOrderBase>> GetOpenPositions(string accountId);
    }
}