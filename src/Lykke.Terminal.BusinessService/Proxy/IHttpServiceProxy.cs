﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Lykke.Terminal.BusinessService.Proxy
{
    public interface IHttpServiceProxy
    {
        Uri EndpointUrl { get; }

        Task<T> Get<T>(string resourcePath);
        Task<T> Get<T>(string resourcePath, IDictionary<string, string> requestParams);
        Task<HttpResponseMessage> Post(string resourcePath, object payload);
        Task<HttpResponseMessage> PostAsForm(string resourcePath, IEnumerable<KeyValuePair<string, string>> formData);
        Task<HttpResponseMessage> Delete(string url);
    }
}