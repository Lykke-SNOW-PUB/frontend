﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Lykke.Terminal.Common.Extenstions;

namespace Lykke.Terminal.BusinessService.Proxy
{
    public class HttpServiceProxy : IHttpServiceProxy
    {
        private readonly HttpClient _httpClient;

        public Uri EndpointUrl => _httpClient.BaseAddress;

        public HttpServiceProxy(string endpointUrl)
        {
            _httpClient = new HttpClient()
            {
                BaseAddress = new Uri(endpointUrl)
            };
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<T> Get<T>(string resourcePath)
        {
            return
                await _httpClient.GetAsObjectAsync<T>(resourcePath);
        }

        public async Task<T> Get<T>(string resourcePath, IDictionary<string, string> requestParams)
        {
            resourcePath += "?";
            foreach (KeyValuePair<string, string> param in requestParams)
            {
                resourcePath += $"{param.Key}={param.Value}&";
            }
            resourcePath.TrimEnd('&');

            return 
                await Get<T>(resourcePath);
        }

        public async Task<HttpResponseMessage> Post(string resourcePath, object payload)
        {
            return
                await _httpClient.PostAsJsonAsync(resourcePath, payload);
        }

        public async Task<HttpResponseMessage> PostAsForm(string resourcePath, IEnumerable<KeyValuePair<string, string>> formData)
        {
            return
                await _httpClient.PostAsync(resourcePath, new FormUrlEncodedContent(formData));
        }

        public async Task<HttpResponseMessage> Delete(string url)
        {
            return
                await _httpClient.DeleteAsync(url);
        }
    }
}