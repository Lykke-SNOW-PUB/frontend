﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Lykke.Terminal.Domain.Accounts;
using Lykke.Terminal.Domain.Exchange;

namespace Lykke.Terminal.BusinessService.Proxy.MatchingEngine
{
    public interface IMatchingEngineProxy : IProxy
    {
        Task<AccountInfo> GetAccountInfoAsync(string accountInfo);
        Task<IEnumerable<OrderBase>> GetActivePositionsAsync(string accountId);
        Task OpenOrderAsync(string accountId, string assetPairId, double volume, double definedPrice);
        Task CloseOrderAsync(string accountId, string orderId);
        Task<IEnumerable<AssetPairQuote>> GetMarketProfileAsync();
        Task<IEnumerable<OrderBook>> GetOrderBookAsync();
        Task<IEnumerable<DoneOrder>> GetTransactionsHistoryAsync(string accountId);
        Task UpdateAccountBalanceAsync(string accountId, double balance);
    }
}