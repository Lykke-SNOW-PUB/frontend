﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Lykke.Terminal.Domain.Accounts;
using Lykke.Terminal.Domain.Exchange;
using WampSharp.V2.Rpc;

namespace Lykke.Terminal.BusinessService.Proxy.MatchingEngine
{
    public class MatchingEngineProxy : IMatchingEngineProxy
    {
        private readonly IHttpServiceProxy _httpServiceProxy;

        public MatchingEngineProxy(IHttpServiceProxy httpServiceProxy)
        {
            _httpServiceProxy = httpServiceProxy;
        }

        [WampProcedure("getAccountInfo")]
        public async Task<AccountInfo> GetAccountInfoAsync(string accountId)
        {
            return
                await _httpServiceProxy.Get<AccountInfo>($"accountinfo/{accountId}");
        }

        public async Task<IEnumerable<OrderBase>> GetActivePositionsAsync(string accountId)
        {
            return
                await _httpServiceProxy.Get<IEnumerable<OrderBase>>($"order/{accountId}");
        }

        public async Task OpenOrderAsync(string accountId, string assetPairId, double volume,
            double definedPrice)
        {
            await _httpServiceProxy.Post("order", new
            {
                AssetPairId = assetPairId,
                Volume = volume,
                DefinedPrice = definedPrice,
                AccountId = "42"
            });
        }

        public async Task CloseOrderAsync(string accountId, string orderId)
        {
            await Task.CompletedTask;
            throw new NotImplementedException();
        }

        [WampProcedure("getMarketProfile")]
        public async Task<IEnumerable<AssetPairQuote>> GetMarketProfileAsync()
        {
            return await _httpServiceProxy.Get<IEnumerable<AssetPairQuote>>("marketprofile");
        }

        [WampProcedure("getOrderBook")]
        public async Task<IEnumerable<OrderBook>> GetOrderBookAsync()
        {
            return await _httpServiceProxy.Get<IEnumerable<OrderBook>>("orderbook");
        }

        public async Task<IEnumerable<DoneOrder>> GetTransactionsHistoryAsync(string accountId)
        {
            return await _httpServiceProxy.Get<IEnumerable<DoneOrder>>("orderhistory");
        }

        public async Task UpdateAccountBalanceAsync(string accountId, double balance)
        {
            await _httpServiceProxy.Post("accountbalance", new {accountId = accountId, balance = balance});
        }
    }
}