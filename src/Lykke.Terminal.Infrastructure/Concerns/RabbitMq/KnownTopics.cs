﻿using System.Collections.Generic;

namespace Lykke.Terminal.Infrastructure.Concerns.RabbitMq
{
    public class KnownTopics
    {
        public static string AccountUpdated = "AccountUpdated";
        public static string AssetPairPriceUpdated = "AssetPairPriceUpdated";
        public static string ActiveOrdersUpdated = "ActiveOrdersUpdated";

        public static IEnumerable<string> AllTopics => new List<string>
        {
            AccountUpdated,
            AssetPairPriceUpdated,
            ActiveOrdersUpdated
        };
    }
}
