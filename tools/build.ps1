cd ..\src
dotnet restore
dotnet build .\Lykke.Terminal.Common --configuration Release --no-dependencies
dotnet build .\Domain\src\Lykke.Terminal.Domain --configuration Release --no-dependencies
dotnet build .\Lykke.Terminal.Messaging --configuration Release --no-dependencies
dotnet build .\Lykke.Terminal.Infrastructure --configuration Release --no-dependencies
dotnet build .\Lykke.Terminal.BusinessService --configuration Release --no-dependencies
dotnet build .\Lykke.Terminal.Router.App --configuration Release --no-dependencies
